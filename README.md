# CSAssignment1
## Created by: Matthew Hess

---
### Project Description:
This first project is set of smaller programs written in c. 

Throughout the project:
  * variables were created and assigned for particular pieces of data
  * scanners were utilized
  * loops were created, and set to print out data
  * various Math functions were used
  
+ This project tested my ability to learn a new programming language. I learned that google is my best friend!

---
### prog1_1.c
   This program is written in c. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog1_1.c is located.
  
   Then:
  * Compile the program using the following line of code: `gcc prog1_1.c -o nameYouWantToSaveProgramAs` (Make sure gcc is installed on your computer)
  * Run the program by typing the following line of code: `./nameYouWantSavedYourProgramAs`

  This Program:
  * Prompts out a title header
  * Then on a new line Greets the user and also shares my name.

---
### prog1_2.c
   This program is written in c. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog1_1.c is located.
   
   Then
  * Compile the program using the following line of code: `gcc prog1_1.c -o nameYouWantToSaveProgramAs` (Make sure gcc is installed on your computer)
  * Run the program by typing the following line of code: `./nameYouWantSavedYourProgramAs`

   This Program:
  * Prompts out a title header
  * Greets the user and asks for their name
  * Then replies back the user back using their input

---
### prog1_3.c
   This program is written in c. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog1_1.c is located.
   
   Then
  * Compile the program using the following line of code: `gcc prog1_1.c -o nameYouWantToSaveProgramAs -lm` (Make sure gcc is installed on your computer)
  * Run the program by typing the following line of code: `./nameYouWantSavedYourProgramAs`

   This Program:
  * Prompts out a title header
  * Asks a user for a non-negative integer
  * Returns the hyperbolic cosine value of that integer

----
### prog1_4.c
   This program is written in c. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog1_1.c is located.
   
   Then
  * Compile the program using the following line of code: `gcc prog1_1.c -o nameYouWantToSaveProgramAs -lm` (Make sure gcc is installed on your computer)
  * Run the program by typing the following line of code, after specifying your target values: `./nameYouWantSavedYourProgramAs numberOfElementsToProduce stepSizeToTakeInAsDegrees`

   This Program:
  * Prompts out a title header
  * Prints the current stepsize value in degrees
  * Prints out the cos value of all stepSizes
