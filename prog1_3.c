#include <stdio.h>
#include <math.h>

int main()
{
float userInput;
float result;

printf("Assignment #1-3, Matthew Hess, Matthew9510@gmail.com \n");
printf("Please input a non-negative integer:");
scanf("%f", &userInput);
result = cosh(userInput*(M_PI/180));
printf("The result of cosh(%f) is: %.3f \n", userInput, result);
}
