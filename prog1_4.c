#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]){

//declarations
int numElements = atoi(argv[1]);
int stepSize = atoi(argv[2]);
float xAxis[numElements];
float yAxis[numElements];
int currentIndex = 0;

while(currentIndex < numElements)
{
xAxis[currentIndex] = stepSize * currentIndex;
yAxis[currentIndex] = cos(currentIndex * stepSize * (M_PI /180)); //make sure its 0.00 for the first time & also limit it to .2decimal points
currentIndex++;
}

printf("Assignment #1-4, Matthew Hess, Matthew9510@gmail.com \n");

currentIndex = 0; //reset counter
while(currentIndex < numElements)
{
printf("%.2f | ", xAxis[currentIndex]);
currentIndex++;
}
printf("\n"); //new line for second array
currentIndex=0; //reset counter again

while(currentIndex < numElements)
{
printf("%.2f | ", yAxis[currentIndex]);
currentIndex++;
}
printf("\n");
}
